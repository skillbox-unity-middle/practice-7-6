﻿using Unity.Entities;
using Unity.Mathematics;

public struct MoveData : IComponentData
{
    public float2 speed;
}
